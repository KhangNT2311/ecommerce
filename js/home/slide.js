var swiper = new Swiper('.swiper-container', {
    loop: true,
    effect: 'fade',
    autoplay: {
        delay: 5000,
    },
});

var swiper1 = new Swiper('.swiper-container-product', {
    slidesPerView: 1,
    spaceBetween: 10,
    // init: false,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    breakpoints: {
        '@0.75': {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        '@1.00': {
            slidesPerView: 3,
            spaceBetween: 20,
        },
        '@1.50': {
            slidesPerView: 4,
            spaceBetween: 30,
        },
        '@2.00': {
            slidesPerView: 4,
            spaceBetween: 30,
        },
    }
});

var swiper2 = new Swiper('.swiper-container-blog', {
    slidesPerView: 1,
    spaceBetween: 10,
    // init: false,
    breakpoints: {
        640: {
            slidesPerView: 1,
            spaceBetween: 20,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 40,
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 50,
        },
    }
});

var swiper3 = new Swiper('.swiper-container-company', {
    slidesPerView: 2,
    spaceBetween: 10,
    // init: false,
    breakpoints: {
        '@0.75': {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        '@1.00': {
            slidesPerView: 3,
            spaceBetween: 20,
        },
        '@1.25': {
            slidesPerView: 4,
            spaceBetween: 50,
        },
        '@2.00': {
            slidesPerView: 6,
            spaceBetween: 50,
        },
    }
});

var mySwiper2 = new Swiper('.swiper-container-list-product', {
    watchSlidesProgress: true,
    watchSlidesVisibility: true,
    slidesPerView: 2,
    spaceBetween: 0,
    breakpoints: {
        '@0.75': {
            slidesPerView: 2,
            spaceBetween: 0,
        },
        '@1.00': {
            slidesPerView: 3,
            spaceBetween: 0,
        },
        '@1.25': {
            slidesPerView: 4,
            spaceBetween: 0,
        },
        '@2.00': {
            slidesPerView: 6,
            spaceBetween: 0,
        },
    }
})
var swiperProduct = new Swiper('.swiper-container-product2', {
    slidesPerView: 1,
    spaceBetween: 10,
    // init: false,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    breakpoints: {
        '@0.75': {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        '@1.00': {
            slidesPerView: 3,
            spaceBetween: 20,
        },
        '@1.50': {
            slidesPerView: 4,
            spaceBetween: 30,
        },
        '@2.00': {
            slidesPerView: 4,
            spaceBetween: 30,
        },
    }
});
$(".nav-link").click(() => {
    var swiperProduct2 = new Swiper('.swiper-container-product2', {
        slidesPerView: 1,
        spaceBetween: 10,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            '@0.75': {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            '@1.00': {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            '@1.50': {
                slidesPerView: 4,
                spaceBetween: 30,
            },
            '@2.00': {
                slidesPerView: 4,
                spaceBetween: 30,
            },
        }
    });
})

// var test = document.querySelector(".slide1");

// test.onchange = (e) => {
//     console.log(e);
// }
// function CheckActiveSlide() {
//     if ($(".slide2").hasClass("swiper-slide-active")) {
//         console.log("asdasd");
//     }
// }
// CheckActiveSlide();
