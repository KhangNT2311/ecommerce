
var hide = true;
$('.search-button-menu').click(() => {
    if (hide) {
        $('.search-input').css("opacity", "1");
        $('.search-input').css("visibility", "visible");
        $('.search-input').css("transform", "translateY(0)");
        hide = false
    }
    else {
        $('.search-input').css("opacity", "0");
        $('.search-input').css("visibility", "hidden");
        $('.search-input').css("transform", "translateY(-50px)");
        hide = true
    }
});
var hide1 = true;
$('.profile-button-menu').click(() => {
    if (hide1) {
        $('.cover-profile-options').addClass("cover-profile-options-active");
        hide1 = false
    }
    else {
        $('.cover-profile-options').removeClass("cover-profile-options-active");
        hide1 = true
    }
});

$(document).scroll(() => {
    var $nav2 = $(".second-nav");
    if (jQuery(window).width() > 768) {
        $nav2.removeClass("second-nav-active")
    }
});

$(".mobile-product").click(() => {
    $('#product-bar-show').css("opacity", "1");
    $('#product-bar-show').css("visibility", "visible");
    $('#product-bar-show').css("transform", "translateX(0)");
})

$(".backtohome-mobile").click(() => {
    $('#product-bar-show').css("opacity", "0");
    $('#product-bar-show').css("visibility", "hidden");
    $('#product-bar-show').css("transform", "translateX(100px)");
})

$(".mobile-brand").click(() => {
    $('.brand-bar').css("opacity", "1");
    $('.brand-bar').css("visibility", "visible");
    $('.brand-bar').css("transform", "translateX(0)");
})

$(".backtohome-mobile-brand").click(() => {
    $('.brand-bar').css("opacity", "0");
    $('.brand-bar').css("visibility", "hidden");
    $('.brand-bar').css("transform", "translateX(100px)");
})

$(".mobile-menu-options").click(() => {
    $('.mobile-menu').css("opacity", "1");
    $('.mobile-menu').css("visibility", "visible");
    $('.mobile-menu').css("transform", "translateX(0)");
})

$(".close-menu").click(() => {
    $('.mobile-menu').css("opacity", "0");
    $('.mobile-menu').css("visibility", "hidden");
    $('.mobile-menu').css("transform", "translateX(100px)");
})

