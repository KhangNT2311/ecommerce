let lastScrollTop = 0;
const header = document.querySelector(".header");
header.classList.add("active");
const debounce = (method, delay) => {
    clearTimeout(method._tId);
    method._tId = setTimeout(() => {
        method();
    }, delay);
}
const handleScroll = (e) => {
    const st = window.pageYOffset || document.documentElement.scrollTop;
    const header = document.querySelector(".header");
    if (st <= 0) {
        header.classList.add("active");
        return;
    }
    if (st > lastScrollTop) {
        header.classList.remove("active");
    } else {
        header.classList.add("active");
    }
    lastScrollTop = st <= 0 ? 0 : st;
}
window.addEventListener("scroll", () => {
    debounce(handleScroll, 100)
})
